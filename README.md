#Prototype NAIP imagery and SRTM Retrieval tool

##Instructions
1. Install docker for windows if not present
2. Build docker image 
    docker build -t elevation-downloader .
3. Execute download.  The format for the bounds is minlon minlat maxlon maxlat
    docker run -it -v "%CD%/output":"/output" -v "%CD%/get_inputs.py":"/get_inputs.py" elevation-downloader --bounds -116.6333333 34.1833333 -115.66666 34.75000