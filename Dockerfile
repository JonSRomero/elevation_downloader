FROM alpine:edge
RUN apk update
RUN apk add --no-cache python3
RUN apk add --no-cache py-pip
RUN apk add --no-cache gdal-tools
RUN pip install mercantile requests
COPY get_inputs.py /get_inputs.py
ENTRYPOINT ["/usr/bin/python3","get_inputs.py"]