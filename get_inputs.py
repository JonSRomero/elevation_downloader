"""
Retrieve elevation 
Retrieve imagery
"""

import argparse
import json
import os
import shutil
import sys

import requests
import mercantile
import urllib.request

DEFAULT_ELEVATION_ZOOM_LEVEL=11

NAIP_PRODUCTS_TEMPLATE='http://viewer.nationalmap.gov/tnmaccess/api/products'



parser = argparse.ArgumentParser(prog='input_retriever')
parser.add_argument('--bounds', help='minlon,minlat,maxlon,maxlat',required=True,nargs=4)
parser.add_argument('--zoom', help='zoom level',default=DEFAULT_ELEVATION_ZOOM_LEVEL,type=int)
parser.add_argument('--target-dir', help='directory to place outputs ',default='/output',type=str)
args = parser.parse_args()

if not os.path.exists(args.target_dir):
    print(f"{args.target_dir} must exist")
    sys.exit(-1)



print(args.bounds)
bounds = list(map(float, args.bounds))

current_elevation_files = []
for tile in mercantile.tiles(*bounds,zooms=args.zoom):

    url = f'https://s3.amazonaws.com/elevation-tiles-prod/geotiff/{args.zoom}/{tile.x}/{tile.y}.tif'
    target_dir = f'{args.target_dir}/elevation/{args.zoom}/{tile.x}/'
    target_file = f'{target_dir}/{tile.y}.tif'
    print(f"Retrieving {target_file}")
    current_elevation_files.append(target_file)

    if not os.path.exists(target_dir):
        os.makedirs(target_dir)
    if not os.path.exists(target_file):
        urllib.request.urlretrieve(url,target_file)
    else:
        print(f"Skipping {target_file} as it already exists")
    sys.stdout.flush()

# Retile elevation to 4326
if os.path.exists(f'{args.target_dir}/elevation_4326_tiled'):
    shutil.rmtree(f'{args.target_dir}/elevation_4326_tiled')
os.makedirs(f'{args.target_dir}/elevation_4326_tiled')

# Warp EPSG:3857 CRS elevation tiles into 4326 vrt
os.system(f"gdalbuildvrt {args.target_dir}/elevation_3857.vrt {' '.join(current_elevation_files)}")
os.system(f"gdalwarp -t_srs EPSG:4326 {args.target_dir}/elevation_3857.vrt {args.target_dir}/elevation_4326.vrt")

# Retile elevation 4326 vrt
os.system(f"gdal_retile.py -ps 4096 4096 -targetDir '{args.target_dir}/elevation_4326_tiled' {args.target_dir}/elevation_4326.vrt")


# Retrieve overlapping NAIP imagery

naip_datasets = ["USDA National Agriculture Imagery Program (NAIP)"]
naip_matching_products_url = f"{NAIP_PRODUCTS_TEMPLATE}?datasets={','.join(naip_datasets)}&bbox={','.join(map(str,bounds))}"
print(f"Matching naip products url: {naip_matching_products_url}")
matching_naip = requests.get(naip_matching_products_url).json()

if not os.path.exists(f'{args.target_dir}/naip_cache'):
    os.makedirs(f'{args.target_dir}/naip_cache')
current_imagery_files = []

for i,naip_item in enumerate(matching_naip['items'],1):
    print(f"{i} of {len(matching_naip['items'])}: {naip_item['downloadURL']} ",flush=True)
    current_imagery_files.append(f'{args.target_dir}/naip_cache/'+naip_item['downloadURL'].split('/')[-1])
    urllib.request.urlretrieve(naip_item['downloadURL'],f'{args.target_dir}/naip_cache/'+naip_item['downloadURL'].split('/')[-1] )

# Build vrt of all matching NAIP
os.system(f"gdalbuildvrt {args.target_dir}/naip_raw.vrt {' '.join(current_imagery_files)}")
sys.stdout.flush()
# Drop fourth NIR band
os.system(f"gdal_translate -b 1 -b 2 -b 3 -colorInterp_1 Red -colorInterp_2 Green -colorInterp_3 Blue {args.target_dir}/naip_raw.vrt {args.target_dir}/naip_band_dropped.vrt")
sys.stdout.flush()
# Warp imagery to 4326
os.system(f"gdalwarp -dstalpha -t_srs EPSG:4326 {args.target_dir}/naip_band_dropped.vrt {args.target_dir}/naip_rgb_4326.vrt")

# Retile in 4096 pixel chunks
if os.path.exists(f'{args.target_dir}/imagery_4326'):
    shutil.rmtree(f'{args.target_dir}/imagery_4326')
os.makedirs(f'{args.target_dir}/imagery_4326')

print("Retiling NAIP imagery to 4326")
os.system(f"gdal_retile.py -ps 4096 4096 -targetDir {args.target_dir}/imagery_4326 {args.target_dir}/naip_rgb_4326.vrt")

# Retile elevation to 4326
